<?php
/**
 * Options
 *
 * Display the options page and save the options
 *
 * @package WordPress
 * @subpackage Employee Scheduler
 * @since 1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Delete options.
 *
 * Delete options when plugin is deleted.
 *
 * @since 1.0
 *
 */
function wpaesm_delete_plugin_options() {
	delete_option( 'wpaesm_options' );
}

/**
 * Default options.
 *
 * Save default option values when plugin is activated.
 *
 * @since 1.0
 */
function wpaesm_add_defaults() {
	$tmp = get_option( 'wpaesm_options' );
    if( !is_array( $tmp ) ) {
		delete_option( 'wpaesm_options' ); // so we don't have to reset all the 'off' checkboxes too! (don't think this is needed but leave for now)
		$defaultfromname = get_bloginfo('name');
		$defaultfromemail = get_bloginfo('admin_email');
		$arr = array(	"notification_from_name" => $defaultfromname,
						"notification_from_email" => $defaultfromemail,
						"notification_subject" => "You have been scheduled for a work shift",
						"admin_notification_email" => $defaultfromemail,
						"week_starts_on" => "Monday",
						"hours" => "40",
						"otrate" => "1.5",
						"mileage" => ".56",
						"calculate" => "actual",
						"currency" => "USD"
		);
		update_option( 'wpaesm_options', $arr );
	}
}

/**
 * Add menu pages.
 *
 * Add menu pages for Employee Scheduler, subpages for instructions and viewing schedules.
 *
 * @since 1.0
 */
function wpaesm_add_options_page() {
	add_menu_page( 
		'Employee Scheduler', 
		'Employee Scheduler', 
		'manage_options', 
		'/employee-scheduler/options.php', 
		'wpaesm_render_options', 
		'dashicons-admin-generic', 
		87.2317 
	);
	add_submenu_page( '/employee-scheduler/options.php', 'Instructions', 'Instructions', 'manage_options', 'instructions', 'wpaesm_instructions' );
	add_submenu_page( 'edit.php?post_type=shift', 'View Schedules', 'View Schedules', 'manage_options', 'view-schedules', 'wpaesm_view_schedules' );
	do_action( 'wpaesm_add_submenu_pages' );
}

/**
 * Register settings.
 *
 * Create the settings sections and fields.
 *
 * @since 1.0
 */
function wpaesm_options_init() {

	register_setting( 'wpaesm_plugin_options', 'wpaesm_options', 'wpaesm_validate_options' );

	add_settings_section(
		'wpaesm_main_section', 
		__( 'Employee Scheduler Settings', 'employee-scheduler' ), 
		'wpaesm_options_section_callback', 
		'wpaesm_plugin_options'
	);

	// TO DO - for public plugin, prefix all settings
	add_settings_field(
		'notification_from_name',
		__( 'Message Sender (Name)', 'employee-scheduler' ),
		'wpaesm_notification_from_name_render',
		'wpaesm_plugin_options',
		'wpaesm_main_section',
		array( 
			__( 'Email notifications sent to employees will come from this name', 'employee-scheduler' )
		)
	);

	add_settings_field(
		'notification_from_email',
		__( 'Message Sender (Email Address)', 'employee-scheduler' ),
		'wpaesm_notification_from_email_render',
		'wpaesm_plugin_options',
		'wpaesm_main_section',
		array( 
			__( 'Email notifications sent to employees will come from this email address', 'employee-scheduler' )
		)
	);

	add_settings_field(
		'notification_subject',
		__( 'Notification Subject', 'employee-scheduler' ),
		'wpaesm_notification_subject_render',
		'wpaesm_plugin_options',
		'wpaesm_main_section',
		array( 
			__( 'Email notifications sent to employees about scheduled shifts will have this subject', 'employee-scheduler' )
		)
	);

//	add_settings_field(
//		'admin_notify_status',
//		__( 'Shift Status Notification', 'employee-scheduler' ),
//		'wpaesm_admin_notify_status_render',
//		'wpaesm_plugin_options',
//		'wpaesm_main_section',
//		array(
//			__( 'Notify admin when an employee changes shift status', 'employee-scheduler' )
//		)
//	);

	add_settings_field(
		'admin_notify_note',
		__( 'Shift Note Notification', 'employee-scheduler' ),
		'wpaesm_admin_notify_note_render',
		'wpaesm_plugin_options',
		'wpaesm_main_section',
		array( 
			__( 'Notify admin when an employee adds a note to a shift', 'employee-scheduler' )
		)
	);

	add_settings_field(
		'admin_notify_clockout',
		__( 'Clockout Notification', 'employee-scheduler' ),
		'wpaesm_admin_notify_clockout_render',
		'wpaesm_plugin_options',
		'wpaesm_main_section',
		array( 
			__( 'Notify admin when an employee clocks out of a shift', 'employee-scheduler' )
		)
	);

	add_settings_field(
		'admin_notification_email',
		__( 'Admin Notification Email', 'employee-scheduler' ),
		'wpaesm_admin_notification_email_render',
		'wpaesm_plugin_options',
		'wpaesm_main_section',
		array( 
			__( 'Enter the email address that will receive email notifications about employee activities', 'employee-scheduler' )
		)
	);

	add_settings_field(
		'extra_shift_approval',
		__( 'Require Approval for Extra Shifts', 'employee-scheduler' ),
		'wpaesm_extra_shift_approval_render',
		'wpaesm_plugin_options',
		'wpaesm_main_section',
		array( 
			__( 'If this is checked, an administrator will receive an email when an employee enters an extra shift, and the administrator can choose whether or not to approve the extra shift.', 'employee-scheduler' )
		)
	);

	add_settings_field(
		'geolocation',
		__( 'Geolocation', 'employee-scheduler' ),
		'wpaesm_geolocation_render',
		'wpaesm_plugin_options',
		'wpaesm_main_section',
		array( 
			__( 'Check to record the location where employees clock in and out', 'employee-scheduler' )
		)
	);

	add_settings_field(
		'week_starts_on',
		__( 'Week Starts On:', 'employee-scheduler' ),
		'wpaesm_week_starts_on_render',
		'wpaesm_plugin_options',
		'wpaesm_main_section',
		array( 
			__( 'For scheduling purposes, what day does the work-week start on?', 'employee-scheduler' )
		)
	);

	add_settings_field(
		'currency',
		__( 'Currency:', 'employee-scheduler' ),
		'wpaesm_currency_render',
		'wpaesm_plugin_options',
		'wpaesm_main_section',
		array(
			__( 'Select the currency for expenses and wages.', 'employee-scheduler' )
		)
	);

	add_settings_field(
		'currency_position',
		__( 'Currency Position:', 'employee-scheduler' ),
		'wpaesm_currency_position_render',
		'wpaesm_plugin_options',
		'wpaesm_main_section',
		array(
			__( 'Display currency symbol before or after number?', 'employee-scheduler' )
		)
	);

}

/**
 * Render "Notification From Name" setting.
 *
 * @since 1.0
 */
function wpaesm_notification_from_name_render( $args ) {
	$options = get_option( 'wpaesm_options' ); ?>

	<input type="text" size="57" name="wpaesm_options[notification_from_name]" value="<?php echo $options['notification_from_name']; ?>" />
	<br /><span class="description"><?php echo $args[0]; ?></span>

<?php }

/**
 * Render "Notification Email" setting.
 *
 * @since 1.0
 */
function wpaesm_notification_from_email_render( $args ) {
	$options = get_option( 'wpaesm_options' ); ?>

	<input type="text" size="57" name="wpaesm_options[notification_from_email]" value="<?php echo $options['notification_from_email']; ?>" />
	<br /><span class="description"><?php echo $args[0]; ?></span>
<?php }

/**
 * Render "Notification Subject" setting.
 *
 * @since 1.0
 */
function wpaesm_notification_subject_render( $args ) {
	$options = get_option( 'wpaesm_options' ); ?>

	<input type="text" size="57" name="wpaesm_options[notification_subject]" value="<?php echo $options['notification_subject']; ?>" />
	<br /><span class="description"><?php echo $args[0]; ?></span>
<?php }

/**
 * Render "Admin Notify Status" setting.
 *
 * @since 1.0
 */
function wpaesm_admin_notify_status_render( $args ) {
	$options = get_option( 'wpaesm_options' ); ?>

	<label><input name="wpaesm_options[admin_notify_status]" type="checkbox" value="1" <?php if (isset($options['admin_notify_status'])) { checked('1', $options['admin_notify_status']); } ?> /> <?php _e('Turn on shift status notifications', 'employee-scheduler'); ?></label>
	<br /><span class="description"><?php echo $args[0]; ?></span>
<?php }

/**
 * Render "Admin Notify Note" setting.
 *
 * @since 1.0
 */
function wpaesm_admin_notify_note_render( $args ) {
	$options = get_option( 'wpaesm_options' ); ?>

	<label><input name="wpaesm_options[admin_notify_note]" type="checkbox" value="1" <?php if (isset($options['admin_notify_note'])) { checked('1', $options['admin_notify_note']); } ?> /> <?php _e('Turn on shift note notifications', 'employee-scheduler'); ?></label>
	<br /><span class="description"><?php echo $args[0]; ?></span>
<?php }

/**
 * Render "Admin Notify Clockout" setting.
 *
 * @since 1.7.0
 */
function wpaesm_admin_notify_clockout_render( $args ) {
	$options = get_option( 'wpaesm_options' ); ?>

	<label><input name="wpaesm_options[admin_notify_clockout]" type="checkbox" value="1" <?php if (isset($options['admin_notify_clockout'])) { checked('1', $options['admin_notify_clockout']); } ?> /> <?php _e( 'Turn on clockout notifications', 'employee-scheduler' ); ?></label>
	<br /><span class="description"><?php echo $args[0]; ?></span>
<?php }

/**
 * Render "Admin Notification Email" setting.
 *
 * @since 1.0
 */
function wpaesm_admin_notification_email_render( $args ) {
	$options = get_option( 'wpaesm_options' ); ?>

	<input type="text" size="57" name="wpaesm_options[admin_notification_email]" value="<?php echo $options['admin_notification_email']; ?>" />
	<br /><span class="description"><?php echo $args[0]; ?></span>
<?php }

/**
 * Render "Extra Shift Approval" setting.
 *
 * @since 1.7.0
 */
function wpaesm_extra_shift_approval_render( $args ) {
	$options = get_option( 'wpaesm_options' ); ?>

	<label><input name="wpaesm_options[extra_shift_approval]" type="checkbox" value="1" <?php if (isset($options['extra_shift_approval'])) { checked('1', $options['extra_shift_approval']); } ?> /> <?php _e('Require approval for extra shifts', 'employee-scheduler'); ?></label><br />
	<br /><span class="description"><?php echo $args[0]; ?></span>

<?php }

/**
 * Render "Geolocation" setting.
 *
 * @since 1.0
 */
function wpaesm_geolocation_render( $args ) {
	$options = get_option( 'wpaesm_options' ); ?>

	<label><input name="wpaesm_options[geolocation]" type="checkbox" value="1" <?php if (isset($options['geolocation'])) { checked('1', $options['geolocation']); } ?> /> <?php _e('Record location when employees clock in and clock out', 'employee-scheduler'); ?></label>
	<br /><span class="description"><?php echo $args[0]; ?></span>
<?php }

/**
 * Render "Week Starts On" setting.
 *
 * @since 1.0
 */
function wpaesm_week_starts_on_render( $args ) {
	$options = get_option( 'wpaesm_options' ); ?>

	<select name='wpaesm_options[week_starts_on]'>
		<option value='Sunday' <?php selected('Sunday', $options['week_starts_on']); ?>><?php _e('Sunday', 'employee-scheduler'); ?></option>
		<option value='Monday' <?php selected('Monday', $options['week_starts_on']); ?>><?php _e('Monday', 'employee-scheduler'); ?></option>
		<option value='Tuesday' <?php selected('Tuesday', $options['week_starts_on']); ?>><?php _e('Tuesday', 'employee-scheduler'); ?></option>
		<option value='Wednesday' <?php selected('Wednesday', $options['week_starts_on']); ?>><?php _e('Wednesday', 'employee-scheduler'); ?></option>
		<option value='Thursday' <?php selected('Thursday', $options['week_starts_on']); ?>><?php _e('Thursday', 'employee-scheduler'); ?></option>
		<option value='Friday' <?php selected('Friday', $options['week_starts_on']); ?>><?php _e('Friday', 'employee-scheduler'); ?></option>
		<option value='Saturday' <?php selected('Saturday', $options['week_starts_on']); ?>><?php _e('Saturday', 'employee-scheduler'); ?></option>
	</select>
	<br /><span class="description"><?php echo $args[0]; ?></span>
<?php }

/**
 * Render "Currency" setting.
 *
 * @since 1.9
 */
function wpaesm_currency_render( $args ) {
	$options = get_option( 'wpaesm_options' );
	$currencies = wpaesm_currency_list(); ?>

	<select name='wpaesm_options[currency]'>
		<option value='' <?php selected('', $options['currency']); ?>></option>
		<?php foreach( $currencies as $symbol => $name ) { ?>
			<option value='<?php echo $symbol; ?>' <?php selected( $symbol, $options['currency']); ?>><?php echo esc_attr( $name ); ?></option>
		<?php } ?>

	</select>
	<br /><span class="description"><?php echo $args[0]; ?></span>
<?php }


/**
 * Render "Currency Position" setting.
 *
 * @since 1.9
 */
function wpaesm_currency_position_render( $args ) {
	$options = get_option( 'wpaesm_options' ); ?>

	<label><input name="wpaesm_options[currency_position]" type="radio" value="before" <?php if (isset($options['currency_position'])) { checked('before', $options['currency_position']); } ?> /> <?php _e('Before (such as $100.00)', 'wpaesp'); ?></label><br />
	<label><input name="wpaesm_options[currency_position]" type="radio" value="after" <?php if (isset($options['currency_position'])) { checked('after', $options['currency_position']); } ?> /> <?php _e('After (such as 100.00 &euro;)', 'wpaesp'); ?></label>
	<br /><span class="description"><?php echo $args[0]; ?></span>
<?php }



/**
 * Render the plugin settings form.
 *
 * @since 1.0
 */
function wpaesm_render_options() {
	?>

	<div class="wrap">
		<h1><?php _e( 'Employee Scheduler', 'employee-scheduler' ); ?></h1>
		<?php settings_errors(); ?>

		<?php $options = get_option( 'wpaesm_options' );
//		echo '<pre>' . print_r( $options, true ) . '</pre>'; ?>

		<div id="main" style="width: 75%; float: left">
			<form method="post" action="options.php">
				<?php
				settings_fields( 'wpaesm_plugin_options' );
				do_settings_sections( 'wpaesm_plugin_options' );
				submit_button();
				?>
			</form>
		</div>

		<aside style="width: 24%; float: right;">
			<?php do_action( 'wpaesm_options_sidebar' ); ?>
		</aside>

	</div>
	<?php	
}

/**
 * Settings section callback.
 *
 * Doesn't do anything.
 *
 * @since 1.0
 */
function wpaesm_options_section_callback() {

}

/**
 * Validate options.
 *
 * Validate and sanitize settings before saving.
 *
 * @since 1.0
 *
 * @param array  $input  Settings entered into the form.
 * @return array $input  Settings to be saved in the database.
 */
function wpaesm_validate_options( $input ) {
	 // strip html from textboxes
	if( isset( $input['notification_from_name'] ) )
		$input['notification_from_name'] =  wp_filter_nohtml_kses($input['notification_from_name']); 
	if( isset( $input['notification_from_email'] ) )
		$input['notification_from_email'] =  wp_filter_nohtml_kses($input['notification_from_email']);
	if( isset( $input['notification_subject'] ) )
		$input['notification_subject'] =  wp_filter_nohtml_kses($input['notification_subject']);
	if( isset( $input['admin_notification_email'] ) )
		$input['admin_notification_email'] =  wp_filter_nohtml_kses($input['admin_notification_email']);
	if( isset( $input['admin_notify_status'] ) )
		$input['admin_notify_status'] =  wp_filter_nohtml_kses($input['admin_notify_status']);
	if( isset( $input['admin_notify_note'] ) )
		$input['admin_notify_note'] =  wp_filter_nohtml_kses($input['admin_notify_note']);
	if( isset( $input['geolocation'] ) )
		$input['geolocation'] =  wp_filter_nohtml_kses($input['geolocation']);
	if( isset( $input['week_starts_on'] ) )
		$input['week_starts_on'] =  wp_filter_nohtml_kses($input['week_starts_on']);
	if( isset( $input['currency'] ) )
		$input['currency'] =  wp_filter_nohtml_kses($input['currency']);
	if( isset( $input['currency_position'] ) )
		$input['currency_position'] =  wp_filter_nohtml_kses($input['currency_position']);
	
	$input = apply_filters( 'wpaesm_validation_options_filter', $input );

	return $input;
}

/**
 * Display sidebar on options page.
 *
 * Display a sidebar that encourages users to donate or upgrade.
 *
 * @since 1.0
 */
function wpaesm_display_options_sidebar() { ?>
	<h3><?php _e( 'Upgrade to Pro!', 'employee-scheduler' ); ?></h3>
	<p><?php _e( 'Employee Scheduler Pro has more features to make it easier to manage your employees!', 'employee-scheduler' ); ?></p>
	<ul>
		<li>* <?php _e( 'Bulk create shifts', 'employee-scheduler' ); ?></li>
		<li>* <?php _e( 'Bulk edit shifts', 'employee-scheduler' ); ?></li>
		<li>* <?php _e( 'Create payroll reports', 'employee-scheduler' ); ?></li>
		<li>* <?php _e( 'Easily filter shifts and expenses on several criteria', 'employee-scheduler' ); ?></li>
		<li>* <?php _e( 'View report comparing employees\' scheduled hours to hours actually worked', 'employee-scheduler' ); ?></li>
	</ul>
	<p><?php _e( 'Pro users will also get personalized, priority support.', 'employee-scheduler' ); ?></p>
	<p><a href="https://wpalchemists.com/downloads/employee-scheduler-pro/" target="_blank" class="button button-primary">
		<?php _e( 'Uprade to Pro', 'employee-scheduler' ); ?>
	</a></p>
	<h3><?php _e( 'Donate now!', 'employee-scheduler' ); ?></h3>
	<p><?php _e( 'Show your appreciation and support continued development of this plugin.', 'employee-scheduler' ); ?></p>
	<p><a href="https://wpalchemists.com/donate/" target="_blank" class="button button-primary">
		<?php _e( 'Donate', 'employee-scheduler' ); ?>
	</a></p>
<?php }
add_action( 'wpaesm_options_sidebar', 'wpaesm_display_options_sidebar', 10 );

function wpaesm_currency_list() {
	$currencies = array(
		'AED' => __( 'United Arab Emirates dirham', 'employee-scheduler' ),
		'AFN' => __( 'Afghan afghani', 'employee-scheduler' ),
		'ALL' => __( 'Albanian lek', 'employee-scheduler' ),
		'AMD' => __( 'Armenian dram', 'employee-scheduler' ),
		'ANG' => __( 'Netherlands Antillean guilder', 'employee-scheduler' ),
		'AOA' => __( 'Angolan kwanza', 'employee-scheduler' ),
		'ARS' => __( 'Argentine peso', 'employee-scheduler' ),
		'AUD' => __( 'Australian dollar', 'employee-scheduler' ),
		'AWG' => __( 'Aruban florin', 'employee-scheduler' ),
		'AZN' => __( 'Azerbaijani manat', 'employee-scheduler' ),
		'BAM' => __( 'Bosnia and Herzegovina convertible mark', 'employee-scheduler' ),
		'BBD' => __( 'Barbadian dollar', 'employee-scheduler' ),
		'BDT' => __( 'Bangladeshi taka', 'employee-scheduler' ),
		'BGN' => __( 'Bulgarian lev', 'employee-scheduler' ),
		'BHD' => __( 'Bahraini dinar', 'employee-scheduler' ),
		'BIF' => __( 'Burundian franc', 'employee-scheduler' ),
		'BMD' => __( 'Bermudian dollar', 'employee-scheduler' ),
		'BND' => __( 'Brunei dollar', 'employee-scheduler' ),
		'BOB' => __( 'Bolivian boliviano', 'employee-scheduler' ),
		'BRL' => __( 'Brazilian real', 'employee-scheduler' ),
		'BSD' => __( 'Bahamian dollar', 'employee-scheduler' ),
		'BTC' => __( 'Bitcoin', 'employee-scheduler' ),
		'BTN' => __( 'Bhutanese ngultrum', 'employee-scheduler' ),
		'BWP' => __( 'Botswana pula', 'employee-scheduler' ),
		'BYR' => __( 'Belarusian ruble', 'employee-scheduler' ),
		'BZD' => __( 'Belize dollar', 'employee-scheduler' ),
		'CAD' => __( 'Canadian dollar', 'employee-scheduler' ),
		'CDF' => __( 'Congolese franc', 'employee-scheduler' ),
		'CHF' => __( 'Swiss franc', 'employee-scheduler' ),
		'CLP' => __( 'Chilean peso', 'employee-scheduler' ),
		'CNY' => __( 'Chinese yuan', 'employee-scheduler' ),
		'COP' => __( 'Colombian peso', 'employee-scheduler' ),
		'CRC' => __( 'Costa Rican col&oacute;n', 'employee-scheduler' ),
		'CUC' => __( 'Cuban convertible peso', 'employee-scheduler' ),
		'CUP' => __( 'Cuban peso', 'employee-scheduler' ),
		'CVE' => __( 'Cape Verdean escudo', 'employee-scheduler' ),
		'CZK' => __( 'Czech koruna', 'employee-scheduler' ),
		'DJF' => __( 'Djiboutian franc', 'employee-scheduler' ),
		'DKK' => __( 'Danish krone', 'employee-scheduler' ),
		'DOP' => __( 'Dominican peso', 'employee-scheduler' ),
		'DZD' => __( 'Algerian dinar', 'employee-scheduler' ),
		'EGP' => __( 'Egyptian pound', 'employee-scheduler' ),
		'ERN' => __( 'Eritrean nakfa', 'employee-scheduler' ),
		'ETB' => __( 'Ethiopian birr', 'employee-scheduler' ),
		'EUR' => __( 'Euro', 'employee-scheduler' ),
		'FJD' => __( 'Fijian dollar', 'employee-scheduler' ),
		'FKP' => __( 'Falkland Islands pound', 'employee-scheduler' ),
		'GBP' => __( 'Pound sterling', 'employee-scheduler' ),
		'GEL' => __( 'Georgian lari', 'employee-scheduler' ),
		'GGP' => __( 'Guernsey pound', 'employee-scheduler' ),
		'GHS' => __( 'Ghana cedi', 'employee-scheduler' ),
		'GIP' => __( 'Gibraltar pound', 'employee-scheduler' ),
		'GMD' => __( 'Gambian dalasi', 'employee-scheduler' ),
		'GNF' => __( 'Guinean franc', 'employee-scheduler' ),
		'GTQ' => __( 'Guatemalan quetzal', 'employee-scheduler' ),
		'GYD' => __( 'Guyanese dollar', 'employee-scheduler' ),
		'HKD' => __( 'Hong Kong dollar', 'employee-scheduler' ),
		'HNL' => __( 'Honduran lempira', 'employee-scheduler' ),
		'HRK' => __( 'Croatian kuna', 'employee-scheduler' ),
		'HTG' => __( 'Haitian gourde', 'employee-scheduler' ),
		'HUF' => __( 'Hungarian forint', 'employee-scheduler' ),
		'IDR' => __( 'Indonesian rupiah', 'employee-scheduler' ),
		'ILS' => __( 'Israeli new shekel', 'employee-scheduler' ),
		'IMP' => __( 'Manx pound', 'employee-scheduler' ),
		'INR' => __( 'Indian rupee', 'employee-scheduler' ),
		'IQD' => __( 'Iraqi dinar', 'employee-scheduler' ),
		'IRR' => __( 'Iranian rial', 'employee-scheduler' ),
		'ISK' => __( 'Icelandic kr&oacute;na', 'employee-scheduler' ),
		'JEP' => __( 'Jersey pound', 'employee-scheduler' ),
		'JMD' => __( 'Jamaican dollar', 'employee-scheduler' ),
		'JOD' => __( 'Jordanian dinar', 'employee-scheduler' ),
		'JPY' => __( 'Japanese yen', 'employee-scheduler' ),
		'KES' => __( 'Kenyan shilling', 'employee-scheduler' ),
		'KGS' => __( 'Kyrgyzstani som', 'employee-scheduler' ),
		'KHR' => __( 'Cambodian riel', 'employee-scheduler' ),
		'KMF' => __( 'Comorian franc', 'employee-scheduler' ),
		'KPW' => __( 'North Korean won', 'employee-scheduler' ),
		'KRW' => __( 'South Korean won', 'employee-scheduler' ),
		'KWD' => __( 'Kuwaiti dinar', 'employee-scheduler' ),
		'KYD' => __( 'Cayman Islands dollar', 'employee-scheduler' ),
		'KZT' => __( 'Kazakhstani tenge', 'employee-scheduler' ),
		'LAK' => __( 'Lao kip', 'employee-scheduler' ),
		'LBP' => __( 'Lebanese pound', 'employee-scheduler' ),
		'LKR' => __( 'Sri Lankan rupee', 'employee-scheduler' ),
		'LRD' => __( 'Liberian dollar', 'employee-scheduler' ),
		'LSL' => __( 'Lesotho loti', 'employee-scheduler' ),
		'LYD' => __( 'Libyan dinar', 'employee-scheduler' ),
		'MAD' => __( 'Moroccan dirham', 'employee-scheduler' ),
		'MDL' => __( 'Moldovan leu', 'employee-scheduler' ),
		'MGA' => __( 'Malagasy ariary', 'employee-scheduler' ),
		'MKD' => __( 'Macedonian denar', 'employee-scheduler' ),
		'MMK' => __( 'Burmese kyat', 'employee-scheduler' ),
		'MNT' => __( 'Mongolian t&ouml;gr&ouml;g', 'employee-scheduler' ),
		'MOP' => __( 'Macanese pataca', 'employee-scheduler' ),
		'MRO' => __( 'Mauritanian ouguiya', 'employee-scheduler' ),
		'MUR' => __( 'Mauritian rupee', 'employee-scheduler' ),
		'MVR' => __( 'Maldivian rufiyaa', 'employee-scheduler' ),
		'MWK' => __( 'Malawian kwacha', 'employee-scheduler' ),
		'MXN' => __( 'Mexican peso', 'employee-scheduler' ),
		'MYR' => __( 'Malaysian ringgit', 'employee-scheduler' ),
		'MZN' => __( 'Mozambican metical', 'employee-scheduler' ),
		'NAD' => __( 'Namibian dollar', 'employee-scheduler' ),
		'NGN' => __( 'Nigerian naira', 'employee-scheduler' ),
		'NIO' => __( 'Nicaraguan c&oacute;rdoba', 'employee-scheduler' ),
		'NOK' => __( 'Norwegian krone', 'employee-scheduler' ),
		'NPR' => __( 'Nepalese rupee', 'employee-scheduler' ),
		'NZD' => __( 'New Zealand dollar', 'employee-scheduler' ),
		'OMR' => __( 'Omani rial', 'employee-scheduler' ),
		'PAB' => __( 'Panamanian balboa', 'employee-scheduler' ),
		'PEN' => __( 'Peruvian nuevo sol', 'employee-scheduler' ),
		'PGK' => __( 'Papua New Guinean kina', 'employee-scheduler' ),
		'PHP' => __( 'Philippine peso', 'employee-scheduler' ),
		'PKR' => __( 'Pakistani rupee', 'employee-scheduler' ),
		'PLN' => __( 'Polish z&#x142;oty', 'employee-scheduler' ),
		'PRB' => __( 'Transnistrian ruble', 'employee-scheduler' ),
		'PYG' => __( 'Paraguayan guaran&iacute;', 'employee-scheduler' ),
		'QAR' => __( 'Qatari riyal', 'employee-scheduler' ),
		'RON' => __( 'Romanian leu', 'employee-scheduler' ),
		'RSD' => __( 'Serbian dinar', 'employee-scheduler' ),
		'RUB' => __( 'Russian ruble', 'employee-scheduler' ),
		'RWF' => __( 'Rwandan franc', 'employee-scheduler' ),
		'SAR' => __( 'Saudi riyal', 'employee-scheduler' ),
		'SBD' => __( 'Solomon Islands dollar', 'employee-scheduler' ),
		'SCR' => __( 'Seychellois rupee', 'employee-scheduler' ),
		'SDG' => __( 'Sudanese pound', 'employee-scheduler' ),
		'SEK' => __( 'Swedish krona', 'employee-scheduler' ),
		'SGD' => __( 'Singapore dollar', 'employee-scheduler' ),
		'SHP' => __( 'Saint Helena pound', 'employee-scheduler' ),
		'SLL' => __( 'Sierra Leonean leone', 'employee-scheduler' ),
		'SOS' => __( 'Somali shilling', 'employee-scheduler' ),
		'SRD' => __( 'Surinamese dollar', 'employee-scheduler' ),
		'SSP' => __( 'South Sudanese pound', 'employee-scheduler' ),
		'STD' => __( 'S&atilde;o Tom&eacute; and Pr&iacute;ncipe dobra', 'employee-scheduler' ),
		'SYP' => __( 'Syrian pound', 'employee-scheduler' ),
		'SZL' => __( 'Swazi lilangeni', 'employee-scheduler' ),
		'THB' => __( 'Thai baht', 'employee-scheduler' ),
		'TJS' => __( 'Tajikistani somoni', 'employee-scheduler' ),
		'TMT' => __( 'Turkmenistan manat', 'employee-scheduler' ),
		'TND' => __( 'Tunisian dinar', 'employee-scheduler' ),
		'TOP' => __( 'Tongan pa&#x2bb;anga', 'employee-scheduler' ),
		'TRY' => __( 'Turkish lira', 'employee-scheduler' ),
		'TTD' => __( 'Trinidad and Tobago dollar', 'employee-scheduler' ),
		'TWD' => __( 'New Taiwan dollar', 'employee-scheduler' ),
		'TZS' => __( 'Tanzanian shilling', 'employee-scheduler' ),
		'UAH' => __( 'Ukrainian hryvnia', 'employee-scheduler' ),
		'UGX' => __( 'Ugandan shilling', 'employee-scheduler' ),
		'USD' => __( 'United States dollar', 'employee-scheduler' ),
		'UYU' => __( 'Uruguayan peso', 'employee-scheduler' ),
		'UZS' => __( 'Uzbekistani som', 'employee-scheduler' ),
		'VEF' => __( 'Venezuelan bol&iacute;var', 'employee-scheduler' ),
		'VND' => __( 'Vietnamese &#x111;&#x1ed3;ng', 'employee-scheduler' ),
		'VUV' => __( 'Vanuatu vatu', 'employee-scheduler' ),
		'WST' => __( 'Samoan t&#x101;l&#x101;', 'employee-scheduler' ),
		'XAF' => __( 'Central African CFA franc', 'employee-scheduler' ),
		'XCD' => __( 'East Caribbean dollar', 'employee-scheduler' ),
		'XOF' => __( 'West African CFA franc', 'employee-scheduler' ),
		'XPF' => __( 'CFP franc', 'employee-scheduler' ),
		'YER' => __( 'Yemeni rial', 'employee-scheduler' ),
		'ZAR' => __( 'South African rand', 'employee-scheduler' ),
		'ZMW' => __( 'Zambian kwacha', 'employee-scheduler' ),
	);

	return $currencies;
}

function wpaesm_currency_symbol() {

	$options = get_option( 'wpaesm_options' );
	$currency = $options['currency'];
	if( !isset( $currency ) || '' == $currency ) {
		$currency = 'USD';
	}

	$symbols = array(
		'AED' => '&#x62f;.&#x625;',
		'AFN' => '&#x60b;',
		'ALL' => 'L',
		'AMD' => 'AMD',
		'ANG' => '&fnof;',
		'AOA' => 'Kz',
		'ARS' => '&#36;',
		'AUD' => '&#36;',
		'AWG' => '&fnof;',
		'AZN' => 'AZN',
		'BAM' => 'KM',
		'BBD' => '&#36;',
		'BDT' => '&#2547;&nbsp;',
		'BGN' => '&#1083;&#1074;.',
		'BHD' => '.&#x62f;.&#x628;',
		'BIF' => 'Fr',
		'BMD' => '&#36;',
		'BND' => '&#36;',
		'BOB' => 'Bs.',
		'BRL' => '&#82;&#36;',
		'BSD' => '&#36;',
		'BTC' => '&#3647;',
		'BTN' => 'Nu.',
		'BWP' => 'P',
		'BYR' => 'Br',
		'BZD' => '&#36;',
		'CAD' => '&#36;',
		'CDF' => 'Fr',
		'CHF' => '&#67;&#72;&#70;',
		'CLP' => '&#36;',
		'CNY' => '&yen;',
		'COP' => '&#36;',
		'CRC' => '&#x20a1;',
		'CUC' => '&#36;',
		'CUP' => '&#36;',
		'CVE' => '&#36;',
		'CZK' => '&#75;&#269;',
		'DJF' => 'Fr',
		'DKK' => 'DKK',
		'DOP' => 'RD&#36;',
		'DZD' => '&#x62f;.&#x62c;',
		'EGP' => 'EGP',
		'ERN' => 'Nfk',
		'ETB' => 'Br',
		'EUR' => '&euro;',
		'FJD' => '&#36;',
		'FKP' => '&pound;',
		'GBP' => '&pound;',
		'GEL' => '&#x10da;',
		'GGP' => '&pound;',
		'GHS' => '&#x20b5;',
		'GIP' => '&pound;',
		'GMD' => 'D',
		'GNF' => 'Fr',
		'GTQ' => 'Q',
		'GYD' => '&#36;',
		'HKD' => '&#36;',
		'HNL' => 'L',
		'HRK' => 'Kn',
		'HTG' => 'G',
		'HUF' => '&#70;&#116;',
		'IDR' => 'Rp',
		'ILS' => '&#8362;',
		'IMP' => '&pound;',
		'INR' => '&#8377;',
		'IQD' => '&#x639;.&#x62f;',
		'IRR' => '&#xfdfc;',
		'ISK' => 'Kr.',
		'JEP' => '&pound;',
		'JMD' => '&#36;',
		'JOD' => '&#x62f;.&#x627;',
		'JPY' => '&yen;',
		'KES' => 'KSh',
		'KGS' => '&#x43b;&#x432;',
		'KHR' => '&#x17db;',
		'KMF' => 'Fr',
		'KPW' => '&#x20a9;',
		'KRW' => '&#8361;',
		'KWD' => '&#x62f;.&#x643;',
		'KYD' => '&#36;',
		'KZT' => 'KZT',
		'LAK' => '&#8365;',
		'LBP' => '&#x644;.&#x644;',
		'LKR' => '&#xdbb;&#xdd4;',
		'LRD' => '&#36;',
		'LSL' => 'L',
		'LYD' => '&#x644;.&#x62f;',
		'MAD' => '&#x62f;. &#x645;.',
		'MAD' => '&#x62f;.&#x645;.',
		'MDL' => 'L',
		'MGA' => 'Ar',
		'MKD' => '&#x434;&#x435;&#x43d;',
		'MMK' => 'Ks',
		'MNT' => '&#x20ae;',
		'MOP' => 'P',
		'MRO' => 'UM',
		'MUR' => '&#x20a8;',
		'MVR' => '.&#x783;',
		'MWK' => 'MK',
		'MXN' => '&#36;',
		'MYR' => '&#82;&#77;',
		'MZN' => 'MT',
		'NAD' => '&#36;',
		'NGN' => '&#8358;',
		'NIO' => 'C&#36;',
		'NOK' => '&#107;&#114;',
		'NPR' => '&#8360;',
		'NZD' => '&#36;',
		'OMR' => '&#x631;.&#x639;.',
		'PAB' => 'B/.',
		'PEN' => 'S/.',
		'PGK' => 'K',
		'PHP' => '&#8369;',
		'PKR' => '&#8360;',
		'PLN' => '&#122;&#322;',
		'PRB' => '&#x440;.',
		'PYG' => '&#8370;',
		'QAR' => '&#x631;.&#x642;',
		'RMB' => '&yen;',
		'RON' => 'lei',
		'RSD' => '&#x434;&#x438;&#x43d;.',
		'RUB' => '&#8381;',
		'RWF' => 'Fr',
		'SAR' => '&#x631;.&#x633;',
		'SBD' => '&#36;',
		'SCR' => '&#x20a8;',
		'SDG' => '&#x62c;.&#x633;.',
		'SEK' => '&#107;&#114;',
		'SGD' => '&#36;',
		'SHP' => '&pound;',
		'SLL' => 'Le',
		'SOS' => 'Sh',
		'SRD' => '&#36;',
		'SSP' => '&pound;',
		'STD' => 'Db',
		'SYP' => '&#x644;.&#x633;',
		'SZL' => 'L',
		'THB' => '&#3647;',
		'TJS' => '&#x405;&#x41c;',
		'TMT' => 'm',
		'TND' => '&#x62f;.&#x62a;',
		'TOP' => 'T&#36;',
		'TRY' => '&#8378;',
		'TTD' => '&#36;',
		'TWD' => '&#78;&#84;&#36;',
		'TZS' => 'Sh',
		'UAH' => '&#8372;',
		'UGX' => 'UGX',
		'USD' => '&#36;',
		'UYU' => '&#36;',
		'UZS' => 'UZS',
		'VEF' => 'Bs F',
		'VND' => '&#8363;',
		'VUV' => 'Vt',
		'WST' => 'T',
		'XAF' => 'Fr',
		'XCD' => '&#36;',
		'XOF' => 'Fr',
		'XPF' => 'Fr',
		'YER' => '&#xfdfc;',
		'ZAR' => '&#82;',
		'ZMW' => 'ZK',
	);
	$currency_symbol = isset( $symbols[ $currency ] ) ? $symbols[ $currency ] : '';
	return $currency_symbol;
}
