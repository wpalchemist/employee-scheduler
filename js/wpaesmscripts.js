jQuery(document).ready(function() {
	jQuery('#thisdate').datetimepicker({
		timepicker:false,
		mask: true,
		format:'Y-m-d'
	});
	jQuery('#thisdate2').datetimepicker({
		timepicker:false,
		mask: true,
		format:'Y-m-d'
	});
	jQuery('#repeatuntil').datetimepicker({
		timepicker:false,
		mask: true,
		format:'Y-m-d'
	});
	jQuery('#starttime').datetimepicker({
		datepicker:false,
		// mask: true,
		format:'H:i',
		step: 15,
	});
	jQuery('#endtime').datetimepicker({
		datepicker:false,
		// mask: true,
		format:'H:i',
		step: 15,
	});
	jQuery('.starttime').datetimepicker({
		datepicker:false,
		// mask: true,
		format:'H:i',
		step: 15,
	});
	jQuery('.endtime').datetimepicker({
		datepicker:false,
		// mask: true,
		format:'H:i',
		step: 15,
	});
	jQuery('#clockin').datetimepicker({
		datepicker:false,
		// mask: true,
		format:'H:i',
		step: 15,
	});
	jQuery('#clockout').datetimepicker({
		datepicker:false,
		// mask: true,
		format:'H:i',
		step: 15,
	});
	jQuery('.shiftee-od-date').datetimepicker({
		timepicker:false,
		mask: false,
		format:'Y-m-d'
	});
	jQuery('.shiftee-od-starttime').datetimepicker({
		datepicker:false,
		// mask: true,
		format:'g:i a',
		step: 15,
	});
	jQuery('.shiftee-od-endtime').datetimepicker({
		datepicker:false,
		// mask: true,
		format:'g:i a',
		step: 15,
	});
	if(jQuery("#repeat").is(':checked'))
		jQuery("#repeatfields").show();  // checked
	else
		jQuery("#repeatfields").hide();  // unchecked
	jQuery('#repeat').onchange = function() {
		jQuery('#repeatfields').style.display = this.checked ? 'block' : 'none';
	};

	var availabilityTemplate = jQuery('#availability-template').html();

	// Add a new repeating section on user profile (only used by On Demand add-on)
	jQuery('.shiftee-repeat-availability').click(function(e){
		e.preventDefault();
		jQuery('.shiftee-od-same-employee').show();
		var repeating = jQuery(availabilityTemplate);
		var lastRepeatingGroup = jQuery('.repeating-availability').last();
		var idx = lastRepeatingGroup.index();
		var attrs = ['for', 'id', 'name'];
		var tags = repeating.find('input, label, select');
		tags.each(function() {
			var section = jQuery(this);
			jQuery.each(attrs, function(i, attr) {
				var attr_val = section.attr(attr);
				if (attr_val) {
					section.attr( attr, attr_val.replace( /\[\d+\]\[/, '\['+( idx + 1 )+'\]\[' ) );
				}
			})
		});

		lastRepeatingGroup.after(repeating);
		repeating.find('.shiftee-od-date').datetimepicker({
			timepicker:false,
			mask: false,
			format:'Y-m-d'
		});
		repeating.find('.shiftee-od-starttime, .shiftee-od-endtime').datetimepicker({
			datepicker:false,
			format:'g:i a',
			step: 15,
		});
		repeating.find('.endtime, .starttime').datetimepicker({
			datepicker:false,
			format:'H:i',
			step: 15,
		});
	});

	jQuery('body').on('click', 'a.remove-availability', function(e){
		e.preventDefault();
		jQuery(this).closest('.repeating-availability').remove();
	});

	jQuery('#shiftee-od-employer-work-request').on( 'submit', function (e){
		jQuery('#shiftee-loading').show();
	});

});



